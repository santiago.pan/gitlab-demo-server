const express = require("express");
const app = express();
const bodyParser = require("body-parser");

const port = process.env.PORT || 3000;

const xss_example1 = `<a href="javascript: alert('XSS Demo!')">XSS attack example 1!</a>`;

const xss_example2 = `<a href="javascript:var xhr = new XMLHttpRequest();
var payload=JSON.stringify({stage:'Malicious Data!'});
xhr.open('POST','https://gitlab-demo-server.herokuapp.com/stages',true);
xhr.setRequestHeader('Content-type','application/json');
xhr.send(payload);">XSS attack example 2!</a>`;

let stages = [
  "Manage",
  "Plan",
  "Create",
  "Verify",
  "Package",
  "Secure",
  "Release",
  "Configure",
  "Monitor",
  "Defend",
  xss_example1,
  xss_example2
];

const cors = function(req, res, next) {
  res.header(
    "Access-Control-Allow-Origin",
    "https://gitlab-demo-web.herokuapp.com"
  );
  res.header("Access-Control-Allow-Methods", "GET,POST");
  res.header("Access-Control-Allow-Headers", "Content-Type");

  next();
};

app.use(cors);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get("/stages", (req, res) => {
  res.setHeader("Content-Type", "application/json");
  res.send({ stages });
});

app.post("/stages", (req, res) => {
  stages.push(req.body.stage);
  res.send({ stages });
});

app.get("/reset", (req, res) => {
  stages = stages.slice(0, 12);
  res.setHeader("Content-Type", "application/json");
  res.send({ stages });
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
